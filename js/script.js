$(document).ready(function(){
    $('.slider__body').slick({
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: false
    });

    $('.carousel-prev').click(function(){
        $('.slider__body').slick('slickPrev');
      });
    
    $('.carousel-next').click(function(){
        $('.slider__body').slick('slickNext');
    });
});